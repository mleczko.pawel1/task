FROM php:8.2-cli
RUN apt update && \
    apt install -y git unzip --no-install-recommends && \
    docker-php-ext-install bcmath && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY . /app
WORKDIR /app

RUN composer install