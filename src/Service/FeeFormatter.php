<?php

declare(strict_types=1);

namespace App\Service;

class FeeFormatter implements FeeFormatterInterface
{
    public function format(float $fee): string
    {
        return number_format($fee, 2, '.', '');
    }
}
