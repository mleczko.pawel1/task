<?php

declare(strict_types=1);

namespace App\Service;

interface FeeFormatterInterface
{
    public function format(float $fee): string;
}
