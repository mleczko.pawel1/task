<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\FileNotFoundException;

final readonly class LocalFileLoaderService implements FileLoaderInterface
{
    public function __construct(
        private string $defaultPath
    ) {
    }

    /**
     * @throws FileNotFoundException
     */
    public function loadFile(string $filePath): array
    {
        $filePath = $this->prepareFullFilePath($filePath);

        if (! file_exists($filePath)) {
            throw new FileNotFoundException($filePath);
        }

        return array_map('str_getcsv', file($filePath));
    }

    private function prepareFullFilePath(string $filePath): string
    {
        if (file_exists($filePath)) {
            return $filePath;
        }

        return sprintf('%s/%s', $this->defaultPath, $filePath);
    }
}
