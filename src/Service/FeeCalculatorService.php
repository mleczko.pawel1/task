<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\Operation;
use App\Model\User;
use App\Service\Operation\DepositService;
use App\Service\Operation\WithdrawService;

readonly class FeeCalculatorService implements FeeCalculatorInterface
{
    public function __construct(
        private DepositService $depositService,
        private WithdrawService $withdrawService
    ) {
    }

    public function calculateFee(Operation $operation, User $user): float
    {
        if ($operation->isDeposit()) {
            $fee = $this->depositService->calculate($operation);
        } else {
            $this->withdrawService->setUser($user);
            $fee = $this->withdrawService->calculate($operation);
        }

        return $fee;
    }
}
