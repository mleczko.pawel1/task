<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class ExchangeRatesClient
{
    public function __construct(
        private readonly HttpClientInterface $client,
        private readonly string $apiUri
    ) {
    }

    public function fetchExchangeRates(): array
    {
        $response = $this->client->request('GET', $this->apiUri);

        return $response->toArray();
    }
}
