<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\Operation;
use App\Model\User;

interface FeeCalculatorInterface
{
    public function calculateFee(Operation $operation, User $user): float;
}
