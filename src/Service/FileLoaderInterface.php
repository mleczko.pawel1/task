<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\FileNotFoundException;

interface FileLoaderInterface
{
    /**
     * @throws FileNotFoundException
     */
    public function loadFile(string $filePath);
}
