<?php

declare(strict_types=1);

namespace App\Service\Operation;

use App\Model\Operation;

interface OperationServiceInterface
{
    public function calculate(Operation $operation): float;
}
