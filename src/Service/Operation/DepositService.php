<?php

declare(strict_types=1);

namespace App\Service\Operation;

use App\Model\Operation;

class DepositService implements OperationServiceInterface
{
    private const DEPOSIT_FEE_RATE = 0.0003;

    public function calculate(Operation $operation): float
    {
        return $operation->getOperationAmount() * self::DEPOSIT_FEE_RATE;
    }
}
