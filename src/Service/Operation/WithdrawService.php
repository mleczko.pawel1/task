<?php

declare(strict_types=1);

namespace App\Service\Operation;

use App\Factory\ExchangeRateCollectionFactory;
use App\Model\Collection\ExchangeRateCollection;
use App\Model\Operation;
use App\Model\User;

class WithdrawService implements OperationServiceInterface, OperationWithUserInterface
{
    private const PRIVATE_WITHDRAW_FEE_RATE = 0.003;
    private const BUSINESS_WITHDRAW_FEE_RATE = 0.005;

    private User $user;

    private ExchangeRateCollection $exchangeRateCollection;

    public function __construct(ExchangeRateCollectionFactory $collectionFactory)
    {
        $this->exchangeRateCollection = $collectionFactory->createExchangeRateCollection();
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function calculate(Operation $operation): float
    {
        $operationExchangeRate = $this->exchangeRateCollection->getByCurrencyCode($operation->getOperationCurrency());
        $this->updateUser($operation);

        $amountInEuro = $operationExchangeRate->convertToEuro($operation->getOperationAmount());
        $feeInEuro = $this->calculateFeeInEuro($amountInEuro);

        $this->updateUserAmountPerWeek($operationExchangeRate->convertToEuro($operation->getOperationAmount()));
        $this->updateUserLimitExceededPerWeek($amountInEuro);

        return $operationExchangeRate->convertFromEuro($feeInEuro);
    }

    private function updateUser(Operation $operation): void
    {
        $this->user->resetWithdrawalsIfNeeded($operation->getDate());
        $this->user->setLastOperationDate($operation->getDate());
    }

    private function calculateFeeInEuro(float $amountInEuro): float
    {
        if ($this->user->isBusiness()) {
            return $amountInEuro * self::BUSINESS_WITHDRAW_FEE_RATE;
        }

        $madeOperation = false;
        if (!$this->user->isLimitExceeded() && $amountInEuro >= User::WITHDRAW_AMOUNT_LIMIT) {
            $amountInEuro -= User::WITHDRAW_AMOUNT_LIMIT;
            $madeOperation = true;
        }

        $amountInEuro += $this->user->getLimitExceededPerWeek();
        $this->user->resetLimitExceededPerWeek();

        if (
            $amountInEuro < User::WITHDRAW_AMOUNT_LIMIT &&
            !$this->user->isLimitExceeded() &&
            !$madeOperation
        ) {
            $this->user->setLimitExceededPerWeek($amountInEuro);
            return 0;
        }

        return $amountInEuro * self::PRIVATE_WITHDRAW_FEE_RATE;
    }

    private function updateUserAmountPerWeek(float $amountInEuro): void
    {
        $this->user->decreaseAmountPerWeek($amountInEuro);
    }

    private function updateUserLimitExceededPerWeek(float $amountInEuro): void
    {
        if ($amountInEuro >= User::WITHDRAW_AMOUNT_LIMIT) {
            $this->user->resetLimitExceededPerWeek();
        } else {
            $this->user->setLimitExceededPerWeek($amountInEuro);
        }
    }
}
