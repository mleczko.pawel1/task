<?php

declare(strict_types=1);

namespace App\Service\Operation;

use App\Model\User;

interface OperationWithUserInterface
{
    public function setUser(User $user): void;
}
