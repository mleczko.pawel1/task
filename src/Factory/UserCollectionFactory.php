<?php

declare(strict_types=1);

namespace App\Factory;

use App\Model\Collection\UserCollection;
use App\Model\Enum\OperationTypeEnum;
use App\Model\Enum\UserTypeEnum;
use App\Model\Operation;
use App\Model\User;
use DateTimeImmutable;

final class UserCollectionFactory
{
    public function createUsersCollection(array $csvData): UserCollection
    {
        $userCollection = new UserCollection();

        foreach ($csvData as $operationId => $row) {
            [$operationDate, $userId, $userType, $operationType, $operationAmount, $operationCurrency] = $row;
            $userId = (int) $userId;

            $user = $userCollection->getById($userId);

            if (!$user) {
                $user = new User($userId, UserTypeEnum::from($userType));
                $userCollection->append($user);

                $user = $userCollection->getById($userId);
            }

            $operation = new Operation(
                $operationId,
                new DateTimeImmutable($operationDate),
                OperationTypeEnum::from($operationType),
                (float) $operationAmount,
                $operationCurrency
            );

            $user->addOperation($operation);
        }

        return $userCollection;
    }
}
