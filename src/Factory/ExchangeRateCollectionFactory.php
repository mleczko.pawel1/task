<?php

declare(strict_types=1);

namespace App\Factory;

use App\Model\Collection\ExchangeRateCollection;
use App\Model\ExchangeRate;
use App\Service\ExchangeRatesClient;
use Exception;

class ExchangeRateCollectionFactory
{
    public function __construct(
        readonly private ExchangeRatesClient $exchangeRatesClient
    ) {
    }

    public function createExchangeRateCollection(): ExchangeRateCollection
    {
        $exchangeRatesCollection = new ExchangeRateCollection();

        try {
            $exchangeRatesArray = $this->exchangeRatesClient->fetchExchangeRates();
        } catch (Exception) {
            return $exchangeRatesCollection;
        }

        foreach ($exchangeRatesArray['rates'] as $currencyCode => $rateToEuro) {
            $exchangeRatesCollection->append(new ExchangeRate($currencyCode, $rateToEuro));
        }

        return $exchangeRatesCollection;
    }
}
