<?php

declare(strict_types=1);

namespace App\Model;

final readonly class ExchangeRate
{
    public function __construct(
        private string $currency,
        private float $rateToEuro
    ) {
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function convertToEuro(float $amount): float
    {
        return $amount / $this->rateToEuro;
    }

    public function convertFromEuro(float $amount): float
    {
        return $amount * $this->rateToEuro;
    }
}
