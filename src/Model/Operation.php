<?php

declare(strict_types=1);

namespace App\Model;

use App\Model\Enum\OperationTypeEnum;
use DateTimeInterface;

final readonly class Operation
{
    public function __construct(
        private int $operationId,
        private DateTimeInterface $date,
        private OperationTypeEnum $operationType,
        private float $operationAmount,
        private string $operationCurrency
    ) {
    }

    /**
     * @return int
     */
    public function getOperationId(): int
    {
        return $this->operationId;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function getOperationAmount(): float
    {
        return $this->operationAmount;
    }

    /**
     * @return string
     */
    public function getOperationCurrency(): string
    {
        return $this->operationCurrency;
    }

    public function isDeposit(): bool
    {
        return $this->operationType === OperationTypeEnum::DEPOSIT;
    }
}
