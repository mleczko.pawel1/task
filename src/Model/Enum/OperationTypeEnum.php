<?php

declare(strict_types=1);

namespace App\Model\Enum;

enum OperationTypeEnum: string
{
    case DEPOSIT = 'deposit';
    case WITHDRAW = 'withdraw';
}
