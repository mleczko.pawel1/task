<?php

declare(strict_types=1);

namespace App\Model\Enum;

enum UserTypeEnum: string
{
    case PRIVATE = 'private';
    case BUSINESS = 'business';
}
