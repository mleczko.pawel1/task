<?php

declare(strict_types=1);

namespace App\Model\Collection;

use App\Model\User;
use ArrayObject;

final class UserCollection extends ArrayObject
{
    public function getById(int $id): ?User
    {
        /** @var User $user */
        foreach ($this as $user) {
            if ($user->getUserId() === $id) {
                return $user;
            }
        }

        return null;
    }

    public function getByOperationId(int $operationId): ?User
    {
        /** @var User $user */
        foreach ($this as $user) {
            if ($user->getOperations()->getById($operationId)) {
                return $user;
            }
        }

        return null;
    }
}
