<?php

declare(strict_types=1);

namespace App\Model\Collection;

use App\Model\ExchangeRate;
use ArrayObject;

class ExchangeRateCollection extends ArrayObject
{
    public function getByCurrencyCode(string $currencyCode): ?ExchangeRate
    {
        /** @var ExchangeRate $exchangeRate */
        foreach ($this as $exchangeRate) {
            if ($exchangeRate->getCurrency() === $currencyCode) {
                return $exchangeRate;
            }
        }

        return null;
    }
}
