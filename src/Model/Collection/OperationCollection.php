<?php

declare(strict_types=1);

namespace App\Model\Collection;

use App\Model\Operation;
use ArrayObject;

final class OperationCollection extends ArrayObject
{
    public function getById(int $id): ?Operation
    {
        /** @var Operation $operation */
        foreach ($this as $operation) {
            if ($operation->getOperationId() === $id) {
                return $operation;
            }
        }

        return null;
    }
}
