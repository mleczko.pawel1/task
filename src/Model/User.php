<?php

declare(strict_types=1);

namespace App\Model;

use App\Model\Collection\OperationCollection;
use App\Model\Enum\UserTypeEnum;
use DateTimeInterface;

final class User
{
    public const WITHDRAW_AMOUNT_LIMIT = 1000;
    private const WITHDRAW_COUNT_LIMIT = 3;
    private int $withdrawCountPerWeek = 0;
    private float $withdrawAmountPerWeek = self::WITHDRAW_AMOUNT_LIMIT;
    private bool $limitExceeded = false;
    private OperationCollection $operations;
    private ?DateTimeInterface $lastOperationDate = null;
    private float $limitExceededPerWeek = 0;

    public function __construct(
        readonly private int $userId,
        readonly private UserTypeEnum $userType
    ) {
        $this->operations = new OperationCollection();
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function isBusiness(): bool
    {
        return $this->userType === UserTypeEnum::BUSINESS;
    }

    public function addOperation(Operation $operation): void
    {
        $this->operations->append($operation);
    }

    public function getOperations(): OperationCollection
    {
        return $this->operations;
    }

    public function isLimitExceeded(): bool
    {
        return $this->limitExceeded;
    }

    public function decreaseAmountPerWeek(float $decreasingAmount): void
    {
        $this->withdrawCountPerWeek++;
        $this->withdrawAmountPerWeek -= $decreasingAmount;

        if ($this->withdrawAmountPerWeek <= 0 || $this->withdrawCountPerWeek > self::WITHDRAW_COUNT_LIMIT) {
            $this->limitExceeded = true;
        }
    }

    public function resetWithdrawals(): void
    {
        $this->withdrawCountPerWeek = 0;
        $this->withdrawAmountPerWeek = self::WITHDRAW_AMOUNT_LIMIT;
        $this->limitExceeded = false;
        $this->limitExceededPerWeek = 0;
    }

    public function getLimitExceededPerWeek(): float
    {
        return $this->limitExceededPerWeek;
    }

    public function setLimitExceededPerWeek(float $limitExceededPerWeek): void
    {
        $this->limitExceededPerWeek = $limitExceededPerWeek;
    }

    public function resetLimitExceededPerWeek(): void
    {
        $this->limitExceededPerWeek = 0;
    }

    public function setLastOperationDate(?DateTimeInterface $lastOperationDate): void
    {
        $this->lastOperationDate = $lastOperationDate;
    }

    public function resetWithdrawalsIfNeeded(DateTimeInterface $operationDate): void
    {
        if (!$this->lastOperationDate) {
            return;
        }

        $sunday = clone $this->lastOperationDate->modify('Sunday this week');

        if ($operationDate > $sunday) {
            $this->resetWithdrawals();
        }
    }
}
