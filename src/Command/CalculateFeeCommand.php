<?php

declare(strict_types=1);

namespace App\Command;

use App\Exception\FileNotFoundException;
use App\Factory\UserCollectionFactory;
use App\Service\FeeCalculatorInterface;
use App\Service\FeeFormatterInterface;
use App\Service\FileLoaderInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:calculate-fee',
    description: 'Calculates fee for operations'
)]
class CalculateFeeCommand extends Command
{
    public function __construct(
        private readonly FileLoaderInterface $fileLoaderService,
        private readonly FeeCalculatorInterface $feeCalculator,
        private readonly FeeFormatterInterface $feeFormatter,
        private readonly UserCollectionFactory $userCollectionFactory
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('filename', InputArgument::REQUIRED, 'File path for csv fee');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $fileName = $input->getArgument('filename');

        try {
            $csvData = $this->fileLoaderService->loadFile($fileName);
        } catch (FileNotFoundException) {
            $output->writeln('File not exists');
            return Command::FAILURE;
        }

        $userCollection = $this->userCollectionFactory->createUsersCollection($csvData);

        foreach ($csvData as $operationId => $row) {
            $user = $userCollection->getByOperationId($operationId);
            $operation = $user->getOperations()->getById($operationId);
            $fee = $this->feeCalculator->calculateFee($operation, $user);
            $output->writeln($this->feeFormatter->format($fee));
        }

        return Command::SUCCESS;
    }
}
