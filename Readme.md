## This is simple app to calculate fee from *.csv file based on few simple rules.

## Before run system, build image with provided command
`docker build -t {image_name} .`

## To run system use provided command
`docker run -it --rm {image_name} bin/console app:calculate-fee input.csv`

## To run test use provided command
`docker run -it --rm {image_name} vendor/bin/phpunit`