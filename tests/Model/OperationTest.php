<?php

declare(strict_types=1);

namespace App\Tests\Model;

use App\Model\Enum\OperationTypeEnum;
use App\Model\Operation;
use PHPUnit\Framework\TestCase;

final class OperationTest extends TestCase
{
    public function testGetters(): void
    {
        $date = new \DateTimeImmutable();
        $operation = new Operation(1, $date, OperationTypeEnum::from('deposit'), 100.0, 'USD');

        $this->assertSame(1, $operation->getOperationId());
        $this->assertSame($date, $operation->getDate());
        $this->assertSame(100.0, $operation->getOperationAmount());
        $this->assertSame('USD', $operation->getOperationCurrency());
        $this->assertTrue($operation->isDeposit());
    }
}
