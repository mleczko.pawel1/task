<?php

declare(strict_types=1);

namespace App\Tests\Model;

use App\Model\ExchangeRate;
use PHPUnit\Framework\TestCase;

final class ExchangeRateTest extends TestCase
{
    public function testGetters(): void
    {
        $exchangeRate = new ExchangeRate('USD', 1.2);

        $this->assertSame('USD', $exchangeRate->getCurrency());
        $this->assertSame(1.2, $exchangeRate->convertFromEuro(1));
        $this->assertSame('0.83', number_format($exchangeRate->convertToEuro(1), 2));
    }
}
