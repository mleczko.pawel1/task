<?php

declare(strict_types=1);

namespace App\Tests\Model;

use App\Model\Collection\OperationCollection;
use App\Model\Enum\OperationTypeEnum;
use App\Model\Enum\UserTypeEnum;
use App\Model\Operation;
use App\Model\User;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        $this->user = new User(1, UserTypeEnum::from('business'));
    }

    public function testAddOperation(): void
    {
        $operation = new Operation(100, new DateTimeImmutable(), OperationTypeEnum::from('deposit'), 100.00, 'GPB');
        $this->user->addOperation($operation);

        $this->assertEquals(new OperationCollection([$operation]), $this->user->getOperations());
    }

    public function testDecreaseAmountPerWeek(): void
    {
        $this->user->decreaseAmountPerWeek(100);
        $this->user->decreaseAmountPerWeek(200);
        $this->user->decreaseAmountPerWeek(300);
        $this->user->decreaseAmountPerWeek(500);

        $this->assertTrue($this->user->isLimitExceeded());
    }

    public function testLimitExceededPerWeek(): void
    {
        $this->user->setLimitExceededPerWeek(10);
        $this->user->resetLimitExceededPerWeek();

        $this->assertEquals(0, $this->user->getLimitExceededPerWeek());
    }

    public function testResetWithdrawals(): void
    {
        $this->user->decreaseAmountPerWeek(500);
        $this->user->resetWithdrawals();

        $this->assertFalse($this->user->isLimitExceeded());
        $this->assertEquals(0, $this->user->getLimitExceededPerWeek());
    }

    public function testResetWithdrawalsIfNeeded(): void
    {
        $this->user->resetWithdrawalsIfNeeded(new DateTimeImmutable('2022-05-16'));
        $this->user->decreaseAmountPerWeek(500);
        $this->user->setLastOperationDate(new DateTimeImmutable('2023-05-09'));
        $this->user->resetWithdrawalsIfNeeded(new DateTimeImmutable('2023-05-16'));

        $this->assertFalse($this->user->isLimitExceeded());
        $this->assertEquals(0, $this->user->getLimitExceededPerWeek());
    }
}
