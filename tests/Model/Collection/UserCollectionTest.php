<?php

declare(strict_types=1);

namespace App\Tests\Model\Collection;

use App\Model\Collection\OperationCollection;
use App\Model\Collection\UserCollection;
use App\Model\Enum\OperationTypeEnum;
use App\Model\Enum\UserTypeEnum;
use App\Model\Operation;
use App\Model\User;
use PHPUnit\Framework\TestCase;

final class UserCollectionTest extends TestCase
{
    public function testGetByIdReturnsUserIfExists(): void
    {
        // Arrange
        $userCollection = new UserCollection([
            new User(1, UserTypeEnum::from('private')),
            new User(2, UserTypeEnum::from('private')),
            new User(3, UserTypeEnum::from('business')),
        ]);

        // Act
        $user = $userCollection->getById(2);

        // Assert
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals(2, $user->getUserId());
        $this->assertFalse($user->isBusiness());
        $this->assertInstanceOf(OperationCollection::class, $user->getOperations());
    }

    public function testGetByIdReturnsNullIfUserDoesNotExist(): void
    {
        // Arrange
        $userCollection = new UserCollection([
            new User(1, UserTypeEnum::from('private')),
            new User(2, UserTypeEnum::from('private')),
            new User(3, UserTypeEnum::from('business')),
        ]);

        // Act
        $user = $userCollection->getById(4);

        // Assert
        $this->assertNull($user);
    }

    public function testGetByOperationIdReturnsUserIfExists(): void
    {
        // Arrange
        $user1 = new User(1, UserTypeEnum::from('private'));
        $user1->addOperation(new Operation(1, new \DateTimeImmutable(), OperationTypeEnum::from('deposit'), 100.00, 'EUR'));
        $user1->addOperation(new Operation(2, new \DateTimeImmutable(), OperationTypeEnum::from('deposit'), 100.00, 'EUR'));

        $user2 = new User(2, UserTypeEnum::from('business'));
        $user2->addOperation(new Operation(3, new \DateTimeImmutable(), OperationTypeEnum::from('withdraw'), 100.00, 'EUR'));
        $user2->addOperation(new Operation(5, new \DateTimeImmutable(), OperationTypeEnum::from('deposit'), 100.00, 'EUR'));

        $user3 = new User(3, UserTypeEnum::from('private'));
        $user3->addOperation(new Operation(4, new \DateTimeImmutable(), OperationTypeEnum::from('withdraw'), 100.00, 'EUR'));
        $user3->addOperation(new Operation(6, new \DateTimeImmutable(), OperationTypeEnum::from('withdraw'), 100.00, 'EUR'));

        $userCollection = new UserCollection([$user1, $user2, $user3]);

        // Act
        $user = $userCollection->getByOperationId(3);

        // Assert
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals(2, $user->getUserId());
        $this->assertTrue($user->isBusiness());
        $this->assertInstanceOf(OperationCollection::class, $user->getOperations());
    }

    public function testGetByOperationIdReturnsNullIfUserDoesNotExist(): void
    {
        $user1 = new User(1, UserTypeEnum::from('private'));
        $user1->addOperation(new Operation(1, new \DateTimeImmutable(), OperationTypeEnum::from('deposit'), 100.00, 'EUR'));
        $user1->addOperation(new Operation(2, new \DateTimeImmutable(), OperationTypeEnum::from('deposit'), 100.00, 'EUR'));

        $user2 = new User(2, UserTypeEnum::from('business'));
        $user2->addOperation(new Operation(3, new \DateTimeImmutable(), OperationTypeEnum::from('withdraw'), 100.00, 'EUR'));
        $user2->addOperation(new Operation(5, new \DateTimeImmutable(), OperationTypeEnum::from('deposit'), 100.00, 'EUR'));

        $user3 = new User(3, UserTypeEnum::from('private'));
        $user3->addOperation(new Operation(4, new \DateTimeImmutable(), OperationTypeEnum::from('withdraw'), 100.00, 'EUR'));
        $user3->addOperation(new Operation(6, new \DateTimeImmutable(), OperationTypeEnum::from('withdraw'), 100.00, 'EUR'));

        // Arrange
        $userCollection = new UserCollection([$user1, $user2, $user3]);

        // Act
        $user = $userCollection->getByOperationId(8);

        // Assert
        $this->assertNull($user);
    }
}
