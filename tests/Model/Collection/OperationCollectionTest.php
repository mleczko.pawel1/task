<?php

declare(strict_types=1);

namespace App\Tests\Model\Collection;

use App\Model\Collection\OperationCollection;
use App\Model\Enum\OperationTypeEnum;
use App\Model\Operation;
use PHPUnit\Framework\TestCase;

final class OperationCollectionTest extends TestCase
{
    public function testGetByIdReturnsOperationIfExists(): void
    {
        // Arrange
        $operationCollection = new OperationCollection([
            new Operation(1, new \DateTimeImmutable(), OperationTypeEnum::from('deposit'), 100.00, 'EUR'),
            new Operation(2, new \DateTimeImmutable(), OperationTypeEnum::from('withdraw'), 50.00, 'GBP'),
        ]);

        // Act
        $operation = $operationCollection->getById(2);

        // Assert
        $this->assertInstanceOf(Operation::class, $operation);
        $this->assertEquals(2, $operation->getOperationId());
        $this->assertFalse($operation->isDeposit());
        $this->assertEquals(50.00, $operation->getOperationAmount());
        $this->assertEquals('GBP', $operation->getOperationCurrency());
    }

    public function testGetByIdReturnsNullIfOperationDoesNotExist(): void
    {
        // Arrange
        $operationCollection = new OperationCollection([
            new Operation(1, new \DateTimeImmutable(), OperationTypeEnum::from('deposit'), 100.00, 'PLN'),
            new Operation(2, new \DateTimeImmutable(), OperationTypeEnum::from('withdraw'), 50.00, 'USD'),
        ]);

        // Act
        $operation = $operationCollection->getById(3);

        // Assert
        $this->assertNull($operation);
    }
}
