<?php

declare(strict_types=1);

namespace App\Tests\Model\Collection;

use App\Model\Collection\ExchangeRateCollection;
use App\Model\ExchangeRate;
use PHPUnit\Framework\TestCase;

final class ExchangeRateCollectionTest extends TestCase
{
    public function testGetByCurrencyCodeReturnsExchangeRateIfExists(): void
    {
        // Arrange
        $exchangeRateCollection = new ExchangeRateCollection([
            new ExchangeRate('USD', 1.21),
            new ExchangeRate('GBP', 0.86),
            new ExchangeRate('JPY', 131.31),
        ]);

        // Act
        $exchangeRate = $exchangeRateCollection->getByCurrencyCode('GBP');

        // Assert
        $this->assertInstanceOf(ExchangeRate::class, $exchangeRate);
        $this->assertEquals('GBP', $exchangeRate->getCurrency());
    }

    public function testGetByCurrencyCodeReturnsNullIfExchangeRateDoesNotExist(): void
    {
        // Arrange
        $exchangeRateCollection = new ExchangeRateCollection([
            new ExchangeRate('USD', 1.21),
            new ExchangeRate('GBP', 0.86),
            new ExchangeRate('JPY', 131.31),
        ]);

        // Act
        $exchangeRate = $exchangeRateCollection->getByCurrencyCode('CAD');

        // Assert
        $this->assertNull($exchangeRate);
    }
}
