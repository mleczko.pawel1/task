<?php

declare(strict_types=1);

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CalculateFeeCommandTest extends KernelTestCase
{
    private CommandTester $commandTester;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $command = $application->find('app:calculate-fee');
        $this->commandTester = new CommandTester($command);

        parent::setUp();
    }

    public function testSuccessExecute(): void
    {
        $this->commandTester->execute([
            'filename' => 'input.csv'
        ]);

        $this->commandTester->assertCommandIsSuccessful();

        $output = $this->commandTester->getDisplay();

        $expected = "0.60\n3.00\n0.00\n0.06\n1.50\n0.00\n0.69\n0.30\n0.57\n3.00\n0.00\n0.00\n8607.39\n";

        $this->assertEquals($expected, $output);
    }

    public function testFailureExecute(): void
    {
        $this->commandTester->execute([
            'filename' => 'failure.csv'
        ]);

        $output = $this->commandTester->getDisplay();

        $this->assertEquals("File not exists\n", $output);
    }
}

