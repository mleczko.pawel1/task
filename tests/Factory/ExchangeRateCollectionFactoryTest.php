<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Factory\ExchangeRateCollectionFactory;
use App\Model\Collection\ExchangeRateCollection;
use App\Model\ExchangeRate;
use App\Service\ExchangeRatesClient;
use PHPUnit\Framework\TestCase;

final class ExchangeRateCollectionFactoryTest extends TestCase
{
    public function testCreateExchangeRateCollection(): void
    {
        // Arrange
        $exchangeRatesArray = [
            'rates' => [
                'USD' => 1.21,
                'GBP' => 0.86,
                'JPY' => 131.31,
            ],
        ];
        $exchangeRatesClient = $this->createMock(ExchangeRatesClient::class);
        $exchangeRatesClient->expects($this->once())
            ->method('fetchExchangeRates')
            ->willReturn($exchangeRatesArray);
        $exchangeRateCollectionFactory = new ExchangeRateCollectionFactory($exchangeRatesClient);

        // Act
        $exchangeRateCollection = $exchangeRateCollectionFactory->createExchangeRateCollection();

        // Assert
        $this->assertInstanceOf(ExchangeRateCollection::class, $exchangeRateCollection);
        $this->assertCount(3, $exchangeRateCollection);

        $expectedExchangeRates = [
            new ExchangeRate('USD', 1.21),
            new ExchangeRate('GBP', 0.86),
            new ExchangeRate('JPY', 131.31),
        ];
        foreach ($exchangeRateCollection as $key => $exchangeRate) {
            $this->assertEquals($expectedExchangeRates[$key], $exchangeRate);
        }
    }
}
