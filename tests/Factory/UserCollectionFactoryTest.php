<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Factory\UserCollectionFactory;
use App\Model\Collection\UserCollection;
use App\Model\Enum\OperationTypeEnum;
use App\Model\Enum\UserTypeEnum;
use App\Model\Operation;
use App\Model\User;
use PHPUnit\Framework\TestCase;

final class UserCollectionFactoryTest extends TestCase
{
    public function testCreateUsersCollection(): void
    {
        $csvData = [
            ['2022-01-01', '1', 'private', 'deposit', '100.00', 'USD'],
            ['2022-01-02', '1', 'private', 'withdraw', '50.00', 'USD'],
            ['2022-01-03', '2', 'business','withdraw', '5000.00', 'EUR'],
            ['2022-01-04', '2', 'business','deposit', '500.00', 'EUR'],
            ['2022-01-05', '1', 'private', 'withdraw', '25.00', 'USD'],
            ['2022-01-06', '1', 'private', 'withdraw', '75.00', 'USD'],
        ];

        $expectedUsersCollection = new UserCollection();
        $user1 = new User(1, UserTypeEnum::from('private'));
        $user1->addOperation(new Operation(0, new \DateTimeImmutable('2022-01-01'), OperationTypeEnum::from('deposit'), 100.0, 'USD'));
        $user1->addOperation(new Operation(1, new \DateTimeImmutable('2022-01-02'), OperationTypeEnum::from('withdraw'), 50.0, 'USD'));
        $user1->addOperation(new Operation(4, new \DateTimeImmutable('2022-01-05'), OperationTypeEnum::from('withdraw'), 25.0, 'USD'));
        $user1->addOperation(new Operation(5, new \DateTimeImmutable('2022-01-06'), OperationTypeEnum::from('withdraw'), 75.0, 'USD'));
        $expectedUsersCollection->append($user1);

        $user2 = new User(2, UserTypeEnum::from('business'));
        $user2->addOperation(new Operation(2, new \DateTimeImmutable('2022-01-03'), OperationTypeEnum::from('withdraw'), 5000.0, 'EUR'));
        $user2->addOperation(new Operation(3, new \DateTimeImmutable('2022-01-04'), OperationTypeEnum::from('deposit'), 500.0, 'EUR'));
        $expectedUsersCollection->append($user2);

        $factory = new UserCollectionFactory();
        $actualUsersCollection = $factory->createUsersCollection($csvData);

        $this->assertEquals($expectedUsersCollection, $actualUsersCollection);
    }
}
