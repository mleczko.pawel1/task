<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\FeeFormatter;
use PHPUnit\Framework\TestCase;

class FeeFormatterTest extends TestCase
{
    private FeeFormatter $formatter;

    protected function setUp(): void
    {
        parent::setUp();

        $this->formatter = new FeeFormatter();
    }

    /**
     * @param float $given
     * @param string $expected
     * @return void
     * @dataProvider differentFeesDataProvider
     */
    public function testFormatToTwoDecimals(float $given, string $expected): void
    {
        $formattedFee = $this->formatter->format($given);

        $this->assertSame($expected, $formattedFee);
    }

    public static function differentFeesDataProvider(): array
    {
        return [
            'small decimals' => [
                'given' => 12.12,
                'expected' => '12.12'
            ],
            'none fee' => [
                'given' => 0.0,
                'expected' => '0.00'
            ],
            'big decimals' => [
                'given' => 12.12121212,
                'expected' => '12.12'
            ]
        ];
    }
}
