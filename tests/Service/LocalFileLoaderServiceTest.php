<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Exception\FileNotFoundException;
use App\Service\LocalFileLoaderService;
use PHPUnit\Framework\TestCase;

class LocalFileLoaderServiceTest extends TestCase
{
    private const DEFAULT_PATH = __DIR__ . '/../fixtures';

    private LocalFileLoaderService $fileLoader;

    protected function setUp(): void
    {
        $this->fileLoader = new LocalFileLoaderService(self::DEFAULT_PATH);
    }

    public function testLoadFile(): void
    {
        $expectedResult = [
            ['1', 'John', '30'],
            ['2', 'Jane', '25'],
            ['3', 'Mark', '40'],
        ];

        $result = $this->fileLoader->loadFile('test.csv');

        $this->assertEquals($expectedResult, $result);
    }

    public function testLoadNonExistingFile(): void
    {
        $this->expectException(FileNotFoundException::class);

        $this->fileLoader->loadFile('non_existing_file.csv');
    }
}
