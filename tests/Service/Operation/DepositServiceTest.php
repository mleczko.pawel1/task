<?php

declare(strict_types=1);

namespace App\Tests\Service\Operation;

use App\Model\Enum\OperationTypeEnum;
use App\Model\Operation;
use App\Service\Operation\DepositService;
use PHPUnit\Framework\TestCase;

final class DepositServiceTest extends TestCase
{
    private DepositService $depositService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->depositService = new DepositService();
    }

    /**
     * @param float $operationAmount
     * @param float $expectedFee
     * @return void
     * @dataProvider depositAmountDataProvider
     */
    public function testCalculate(float $operationAmount, float $expectedFee): void
    {
        $operation = new Operation(1, new \DateTimeImmutable(), OperationTypeEnum::from('deposit'), $operationAmount, 'BUSD');

        $this->assertEquals($expectedFee, $this->depositService->calculate($operation));
    }

    public static function depositAmountDataProvider(): array
    {
        return [
            'deposit with amount greater than zero' => [
                'operationAmount' => 100.0,
                'expectedFee' => 0.03
            ],
            'deposit with amount equals zero' => [
                'operationAmount' => 0,
                'expectedFee' => 0
            ],
        ];
    }
}
