<?php

declare(strict_types=1);

namespace App\Tests\Service\Operation;

use App\Factory\ExchangeRateCollectionFactory;
use App\Model\Collection\ExchangeRateCollection;
use App\Model\Enum\OperationTypeEnum;
use App\Model\Enum\UserTypeEnum;
use App\Model\ExchangeRate;
use App\Model\Operation;
use App\Model\User;
use App\Service\Operation\WithdrawService;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;

class WithdrawServiceTest extends TestCase
{
    /**
     * @param float $baseAmount
     * @param float $convertedToEuroAmount
     * @param float $expectedFee
     * @param string $userType
     * @return void
     * @throws Exception
     * @dataProvider privateUserWithOneOperationDataProvider
     */
    public function testCalculatePrivateWithdrawalFee(
        float $baseAmount,
        float $convertedToEuroAmount,
        float $expectedFee,
        string $userType
    ): void {
        $exchangeRateCollectionFactory = $this->createMock(ExchangeRateCollectionFactory::class);
        $exchangeRateCollection = $this->createMock(ExchangeRateCollection::class);

        $exchangeRateCollection
            ->expects($this->once())
            ->method('getByCurrencyCode')
            ->with('USD')
            ->willReturn(new ExchangeRate('USD', 1.1497));

        $exchangeRateCollectionFactory
            ->expects($this->once())
            ->method('createExchangeRateCollection')
            ->willReturn($exchangeRateCollection);

        $withdrawService = new WithdrawService($exchangeRateCollectionFactory);

        $withdrawService->setUser(new User(1, UserTypeEnum::from($userType)));

        $operation = new Operation(
            1,
            new \DateTimeImmutable(),
            OperationTypeEnum::from('withdraw'),
            $baseAmount,
            'USD'
        );

        $this->assertEquals($expectedFee, $withdrawService->calculate($operation));
    }

    public static function privateUserWithOneOperationDataProvider(): array
    {
        return [
            'no fee private user' => [
                'baseAmount' => 500.0,
                'convertedToEuroAmount' => 434.896059842,
                'expectedFee' => 0.0,
                'userType' => 'private'
            ],
            'with fee private user' => [
                'baseAmount' => 1500.0,
                'convertedToEuroAmount' => 1304.6882,
                'expectedFee' => 1.0509000000000002,
                'userType' => 'private'
            ],
            'with fee business user' => [
                'baseAmount' => 500.0,
                'convertedToEuroAmount' => 434.896059842,
                'expectedFee' => 2.5000000000000004,
                'userType' => 'business'
            ]
        ];
    }
}
