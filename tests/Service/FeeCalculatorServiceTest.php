<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Factory\ExchangeRateCollectionFactory;
use App\Model\Collection\ExchangeRateCollection;
use App\Model\Enum\OperationTypeEnum;
use App\Model\Enum\UserTypeEnum;
use App\Model\ExchangeRate;
use App\Model\Operation;
use App\Model\User;
use App\Service\FeeCalculatorService;
use App\Service\Operation\DepositService;
use App\Service\Operation\WithdrawService;
use PHPUnit\Framework\TestCase;

class FeeCalculatorServiceTest extends TestCase
{
    private const EUR = 'EUR';
    private const USD = 'USD';
    private const AMOUNT = 100;

    private FeeCalculatorService $service;
    private DepositService $depositService;
    private WithdrawService $withdrawService;
    protected function setUp(): void
    {
        $this->depositService = $this->createMock(DepositService::class);
        $this->withdrawService = $this->createMock(WithdrawService::class);

        $exchangeRateCollection = new ExchangeRateCollection([new ExchangeRate(self::USD, 1.2)]);

        $exchangeRateCollectionFactory = $this->createMock(ExchangeRateCollectionFactory::class);
        $exchangeRateCollectionFactory->method('createExchangeRateCollection')
            ->willReturn($exchangeRateCollection);

        $this->withdrawService->method('setUser');

        $this->service = new FeeCalculatorService($this->depositService, $this->withdrawService);
    }

    public function testCalculateFeeForDeposit(): void
    {
        $operation = new Operation(
            1,
            new \DateTimeImmutable(),
            OperationTypeEnum::from('deposit'),
            self::AMOUNT,
            self::EUR
        );

        $user = new User(1, UserTypeEnum::from('private'));
        $user->addOperation($operation);

        $this->depositService->expects(self::once())
            ->method('calculate')
            ->with($operation)
            ->willReturn(0.5);

        $fee = $this->service->calculateFee($operation, $user);

        self::assertSame(0.5, $fee);
    }

    public function testCalculateFeeForWithdraw(): void
    {
        $operation = new Operation(
            1,
            new \DateTimeImmutable(),
            OperationTypeEnum::from('withdraw'),
            self::AMOUNT,
            self::EUR
        );

        $user = new User(1, UserTypeEnum::from('private'));
        $user->addOperation($operation);
        $this->withdrawService->expects(self::once())
            ->method('setUser')
            ->with($user);

        $this->withdrawService->expects(self::once())
            ->method('calculate')
            ->with($operation)
            ->willReturn(1.0);

        $fee = $this->service->calculateFee($operation, $user);

        self::assertSame(1.0, $fee);
    }
}
