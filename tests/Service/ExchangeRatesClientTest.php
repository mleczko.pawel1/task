<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\ExchangeRatesClient;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ExchangeRatesClientTest extends TestCase
{
    public function testFetchExchangeRates(): void
    {
        $expectedResult = ['EUR' => 1.0, 'USD' => 1.22];

        $httpClient = $this->createMock(HttpClientInterface::class);
        $response = $this->createMock(ResponseInterface::class);

        $response->method('toArray')->willReturn($expectedResult);

        $httpClient->method('request')->willReturn($response);

        $client = new ExchangeRatesClient($httpClient, 'https://example.com/api');

        $result = $client->fetchExchangeRates();
        $this->assertSame($expectedResult, $result);
    }
}
